<?php
/**
 * Front Page Template
 *
 */
$primary_landing_background = get_field( 'primary--landing-background' );
$primary_landing_image = $primary_landing_background["url"];
get_header(); ?>

<?php var_dump($primary_landing_background); ?>
<div class="primary--landing-background" style="background-image: url('<?php echo $primary_landing_image; ?>')">
  <div class="container content-wrap index-content" role="main">

    <div class="row">
      <h1><?php the_field( 'landing_page_call_out' ); ?></h1>
    </div>

  </div><!-- end content -->
</div>
<?php // get_sidebar(); ?>

<?php get_footer(); ?>
